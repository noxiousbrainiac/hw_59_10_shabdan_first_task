import React from 'react';
import '../../assets/bootstrap.min.css';
import WatchFilm from "../WatchFilm/WatchFilm";

class FilmList extends React.Component {
    render() {
        return (
            <ul className="p-o my-2">
                {this.props.array ? this.props.array.map((item, index) => (
                    <WatchFilm
                        id={item.id}
                        film={item.film}
                        onDelete={this.props.onDelete}
                        changeFilmName={(e) => this.props.changeFilmName(e ,index)}
                    />
                )) : null}
            </ul>
        );
    }
}

export default FilmList;