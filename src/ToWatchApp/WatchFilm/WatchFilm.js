import React from 'react';
import '../../assets/bootstrap.min.css';

class WatchFilm extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.film !== nextProps.film;
    }

    render() {
        return (
            <li className="card m-2 d-flex flex-row p-2" key={this.props.id}>
                <input
                    type="text"
                    value={this.props.film}
                    className="form-control m-0 mx-2"
                    onChange={this.props.changeFilmName}
                >
                </input>
                <button
                    className="btn btn-primary"
                    type='button'
                    onClick={() => this.props.onDelete(this.props.id)}
                >
                    Remove
                </button>
            </li>
        );
    }
}

export default WatchFilm;