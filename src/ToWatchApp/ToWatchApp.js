import React from 'react';
import '../assets/bootstrap.min.css'
import FilmList from "./FilmList/FilmList";

class ToWatchApp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            array: [],
            inputVal: ''
        };

        const myLocal = localStorage.getItem('array');

        if (myLocal){
            const stateCopy = this.state;
            const arrayCopy = this.state.array;
            arrayCopy.push(JSON.parse(myLocal));
            stateCopy.array = arrayCopy[0];
            this.setState(stateCopy);
        } else {
            localStorage.setItem('array',JSON.stringify([]));
        }
    }

    changeInput (event) {
        this.setState({inputVal: event.target.value});
    }

    addFilm = (event) => {
        if (event.key === "Enter") {
            if (this.state.inputVal){
                const arrayCopy = [...this.state.array];
                arrayCopy.push({id: Date.now(), film: this.state.inputVal})
                this.setState({array: arrayCopy, inputVal: ''});
                localStorage.setItem("array" ,JSON.stringify(arrayCopy));
            }
        }
    }

    onDelete = (id) => {
        const stateCopy = this.state;
        const arrayCopy = [...this.state.array];
        const index = arrayCopy.findIndex(item => item.id === id);
        arrayCopy.splice(index, 1);
        stateCopy.array = arrayCopy;
        this.setState({stateCopy});
        localStorage.setItem("array", JSON.stringify(arrayCopy));
    }

    changeFilmName = (event, index) => {
        const stateCopy = this.state;
        const film = {...this.state.array[index]};
        film.film = event.target.value;

        const arrayCopy = [...this.state.array];
        arrayCopy[index] = film;
        stateCopy.array = arrayCopy;
        this.setState(stateCopy);
        localStorage.setItem("array", JSON.stringify(arrayCopy));
    }

    render() {
        return (
            <>
                <h4>To Watch-list</h4>
                <div className="input-group my-2">
                    <input
                        className="form-control"
                        type="text"
                        value={this.state.inputVal}
                        placeholder="Write your film and press 'Enter' to add in Watch-list"
                        onChange={(event) => this.changeInput(event)}
                        onKeyPress={this.addFilm}
                    />
                </div>
                <FilmList
                    array={this.state.array}
                    onDelete={this.onDelete}
                    changeFilmName={this.changeFilmName}
                />
            </>
        );
    }
}

export default ToWatchApp;