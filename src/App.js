import React from 'react';
import './assets/bootstrap.min.css';
import ToWatchApp from "./ToWatchApp/ToWatchApp";

class App extends React.Component {
    render() {
        return (
            <div className="container p-4">
                <ToWatchApp/>
            </div>
        );
    }
}

export default App;